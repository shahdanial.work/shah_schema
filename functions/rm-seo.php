<?php

define( 'RMU_PLUGIN_FILE', 'seo-by-rank-math/rank-math.php' );
define( 'RMU_PLUGIN_URL', 'https://downloads.wordpress.org/plugin/seo-by-rank-math.latest-stable.zip' );
define( 'RMU_PLUGIN_SLUG', 'seo-by-rank-math' );

$active_plugins = get_option( 'active_plugins' );
$rm_installed   = in_array( RMU_PLUGIN_FILE, $active_plugins, true );
define( 'RMU_INSTALLED', $rm_installed );

/**
 * Suggest Rank Math SEO in notices.
 */
class MTS_RMU {

	private static $instance;
	public $config = array();
	public $plugin;

	private function __construct( $config = array() ) {
		$config_defaults = array(

			// Auto install RM on theme/plugin activation.
			'auto_install'            => false,

			// Auto activate RM on theme/plugin activation.
			'auto_activate'           => false,

			// Don't show wizard when RM is auto-activated.
			'suppress_wizard'         => true,

			'link_label_install'      => __( 'Try it for FREE!', 'schema' ),
			'link_label_activate'     => __( 'Click here to activate it.', 'schema' ),

			// Add tab in MTS Theme Options.
			'add_theme_options_tab'   => true,
			'theme_options_notice'    => '<span style="display: block; line-height: 1.8; margin-bottom: 20px;">' . sprintf( __( '%1$s is a revolutionary SEO product that combines the features of many SEO tools. Its features give you the power of an entire SEO team with just a few clicks.', 'schema' ), '<a href="https://mythemeshop.com/plugins/wordpress-seo-plugin/?utm_source=Theme+Options+Panel&utm_medium=Link+CPC&utm_content=Rank+Math+SEO+LP&utm_campaign=UserBackend" target="_blank">Rank Math SEO</a>' ) . ' @CTA' . '</span>',

			'show_metabox_notice'     => true,

			'add_dashboard_widget'    => false,

			/* Translators: %s is CTA, e.g. "Try it now!" */
			'metabox_notice_install'  => sprintf( __( 'The new %1$s plugin will help you rank better in the search results.', 'schema' ), '<a href="https://mythemeshop.com/plugins/wordpress-seo-plugin/?utm_source=SEO+Meta+Box&utm_medium=Link+CPC&utm_content=Rank+Math+SEO+LP&utm_campaign=UserBackend" target="_blank">Rank Math SEO</a>' ) . ' @CTA',

			/* Translators: %s is CTA, e.g. "Try it now!" */
			'metabox_notice_activate' => sprintf( __( 'The %1$s plugin is installed but not activated.', 'schema' ), '<a href="https://mythemeshop.com/plugins/wordpress-seo-plugin/?utm_source=SEO+Meta+Box&utm_medium=Link+CPC&utm_content=Rank+Math+SEO+LP&utm_campaign=UserBackend" target="_blank">Rank Math SEO</a>' ) . ' @CTA',

			// Add a message in Yoast & AIO metaboxes.
			'show_competitor_notice'  => true,
			'competitor_notice'       =>
				'<span class="dashicons dashicons-lightbulb"></span>
				<span class="mts-ctad-question">' .
					__( 'Did you know?', 'schema' ) . '
				</span>
				<span class="mts-ctad">' .
					sprintf( __( 'The new %1$s plugin can make your site load faster, offers more features, and can import your current SEO settings with one click.', 'schema' ), '<a href="https://mythemeshop.com/plugins/wordpress-seo-plugin/?utm_source=@SOURCE&utm_medium=Link+CPC&utm_content=Rank+Math+SEO+LP&utm_campaign=UserBackend" target="_blank">Rank Math SEO</a>' ) . '
				</span>' . ' @CTA',
		);

		$this->config = $config_defaults;

		// Apply constructor config.
		$this->config( $config );

		$this->add_hooks();
	}

	public function add_hooks() {
		// This needs to run even if RM is installed already.
		// We just suppress the wizard whenever current theme/plugin is activated.
		add_action( 'after_switch_theme', array( $this, 'suppress_redirect' ), 1 );
		$plugin_file = $this->get_plugin_file();
		if ( $plugin_file ) {
			register_activation_hook( $plugin_file, array( $this, 'suppress_redirect' ) );
		}

		// Also, we redirect to RM's Setup Wizard when it is installed from the Recommended Plugins page.
		add_action( 'rank_math_activate', array( $this, 'rm_install_redirect' ) );

		// The rest doesn't need to run when RM is installed already
		// Or if user doesn't have the capability to install plugins.
		if ( RMU_INSTALLED || ! current_user_can( 'install_plugins' ) ) {
			return;
		}
		add_action( 'wp_ajax_rmu_dismiss', array( $this, 'ajax_dismiss_notice' ) );

		if ( $this->get_setting( 'show_competitor_notice' ) ) {
			$active_plugins = get_option( 'active_plugins' );
			if ( in_array( 'wordpress-seo/wp-seo.php', $active_plugins, true ) ) {
				// Add message in Yoast meta box.
				add_action( 'admin_print_footer_scripts-post-new.php', array( $this, 'inject_yoast_notice' ) );
				add_action( 'admin_print_footer_scripts-post.php', array( $this, 'inject_yoast_notice' ) );
			} elseif ( in_array( 'all-in-one-seo-pack/all_in_one_seo_pack.php', $active_plugins, true ) ) {
				// Add message in AIOSEO meta box.
				add_action( 'admin_print_footer_scripts-post-new.php', array( $this, 'inject_aioseo_notice' ) );
				add_action( 'admin_print_footer_scripts-post.php', array( $this, 'inject_aioseo_notice' ) );
			}
		}

		if ( $this->get_setting( 'show_metabox_notice' ) ) {
			$active_plugins = get_option( 'active_plugins' );
			if ( ! in_array( 'wordpress-seo/wp-seo.php', $active_plugins, true ) && ! in_array( 'all-in-one-seo-pack/all_in_one_seo_pack.php', $active_plugins, true ) ) {
				// Add dummy SEO meta box with link to install/activate RM.
				add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			}
		}

		if ( $this->get_setting( 'add_theme_options_tab' ) ) {
			// Add new tab in Theme Options.
			add_filter( 'nhp-opts-sections', array( $this, 'add_theme_options_seo_tab' ) );
		}

		if ( $this->get_setting( 'auto_install' ) ) {
			if ( ! get_option( 'rm_autoinstall', false ) ) {
				add_action( 'after_setup_theme', array( $this, 'autoinstall' ) );
			}
		}

		if ( $this->get_setting( 'add_dashboard_widget' ) ) {
			// Add new tab in Theme Options.
			add_filter( 'wp_dashboard_setup', array( $this, 'add_dashboard_widget' ), 99 );
		}

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_install_scripts' ) );
	}

	public function add_dashboard_widget() {
		if ( $this->is_dismissed( 'dashboard_panel' ) ) {
			return;
		}

		wp_add_dashboard_widget( 'rmu_dashboard_widget', __( 'Rank Math SEO' ), array( $this, 'dashboard_widget_output' ) );
	}

	public function dashboard_widget_output( $post, $callback_args ) {
		$action          = '';
		$url             = '';
		$classes         = '';
		$classic_action  = '';
		$classic_url     = '';
		$classic_classes = '';
		?>
		<div class="rmu-dashboard-panel">
			<a class="rmu-dashboard-panel-close" id="rmu-dashboard-dismiss" href="http://schema.local/wp-admin/?rmu-dashboard=0""><?php _e( 'Dismiss', 'schema' ); ?></a>
			<div class="rmu-dashboard-panel-content">
				<p>
				<?php
				$plugins      = array_keys( get_plugins() );
				$rm_installed = in_array( RMU_PLUGIN_FILE, $plugins, true );

				if ( $rm_installed ) {
						echo strtr( $this->get_setting( 'metabox_notice_activate' ), array( '@CTA' => $this->get_activate_link() ) );
				} else {
						echo strtr( $this->get_setting( 'metabox_notice_install' ), array( '@CTA' => $this->get_install_link() ) );
				}
				?>
				</p>
			</div>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('#rmu_dashboard_widget').insertAfter('.wrap > h1');
				$( '#rmu-dashboard-dismiss' ).click(function(event) {
					event.preventDefault();
					$( '#rmu_dashboard_widget' ).slideUp();
					$.ajax({
						url: ajaxurl,
						type: 'GET',
						data: { action: 'rmu_dismiss', n: 'dashboard_panel' },
					});
				});
			});
		</script>
		<style type="text/css">
			#rmu_dashboard_widget{margin-top:20px;}
			#rmu_dashboard_widget .inside{margin:0;padding:0;}
			#rmu_dashboard_widget .hndle{display:none;}
			.rmu-dashboard-panel .rmu-dashboard-panel-close:before{background:0 0;color:#72777c;content:"\f153";display:block;font:400 16px/20px dashicons;speak:none;height:20px;text-align:center;width:20px;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}
			#rmu_dashboard_widget{position:relative;overflow:auto;border-left:4px solid #ffba00;background:#fffbee;padding:0;box-shadow:0 1px 1px 0 rgba(0,0,0,.1);margin:10px 0;line-height:1.8;}
			.rmu-dashboard-panel h2{margin:0;font-size:21px;font-weight:400;line-height:1.2}
			.rmu-dashboard-panel h3{margin:17px 0 0;font-size:16px;line-height:1.4}
			.rmu-dashboard-panel li{font-size:14px}
			.rmu-dashboard-panel p{color:#72777c}
			.rmu-dashboard-action a{text-decoration:none}
			.rmu-dashboard-panel .about-description{font-size:16px;margin:0}
			.rmu-dashboard-panel-content hr{margin:20px -23px 0;border-top:1px solid #f3f4f5;border-bottom:none}
			.rmu-dashboard-panel .rmu-dashboard-panel-close{position:absolute;z-index:10;top:0;right:10px;padding:0 15px 10px 21px;font-size:13px;line-height:1.23076923;text-decoration:none}
			.rmu-dashboard-panel .rmu-dashboard-panel-close:before{position:absolute;top:0;left:0;transition:all .1s ease-in-out}
			.rmu-dashboard-panel-content{margin:0 13px;max-width:1500px}
			.mts-ctad-question{font-weight:bold;}
		</style>
		<?php
	}

	public function get_plugin_file() {
		if ( ! function_exists( 'debug_backtrace' ) ) {
			return '';
		}

		$backtrace      = debug_backtrace();
		$plugins_folder = trailingslashit( WP_PLUGIN_DIR );
		foreach ( $backtrace as $i => $trace_data ) {
			if ( isset( $trace_data['file'] ) && strpos( $trace_data['file'], $plugins_folder ) === 0 ) {
				// Look for "wp-content/plugins/xx/yy.php".
				$file = str_replace( $plugins_folder, '', $trace_data['file'] );

				if ( preg_match( '#^[^/]+/[^/]+\.php$#i', $file ) ) {
						return $trace_data['file'];
				}
			} else {
				// Backtrace says we're not in a plugin.
				return '';
			}
		}
		return '';
	}

	public function suppress_redirect() {
		if ( get_option( 'rmu_suppress_redirect', false ) ) {
			update_option( 'rank_math_redirect_about', 0 );
			delete_option( 'rmu_suppress_redirect' );
		}
	}

	public function autoinstall() {
		// Only run auto-install once, ever.
		update_option( 'rm_autoinstall', '1' );

		// Check if plugin exists.
		if ( ! function_exists( 'get_plugins' ) ) {
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		$plugins = get_plugins();
		if ( array_key_exists( RMU_PLUGIN_FILE, $plugins ) ) {
				return;
		}

		include_once ABSPATH . 'wp-admin/includes/misc.php';
		include_once ABSPATH . 'wp-admin/includes/file.php';
		include_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
		include_once ABSPATH . 'wp-admin/includes/plugin-install.php';
		$skin     = new Automatic_Upgrader_Skin();
		$upgrader = new Plugin_Upgrader( $skin );

		$result = $upgrader->install( RMU_PLUGIN_URL );

		// @todo: Add notice about auto-install?
		if ( $this->get_setting( 'auto_activate' ) ) {
			$this->autoactivate();
		}
	}

	public function autoactivate() {
		if ( $this->get_setting( 'suppress_wizard' ) ) {
			update_option( 'rank_math_wizard_completed', true );
			update_option( 'rank_math_registration_skip', true );
			update_option( 'rmu_suppress_redirect', '1' );
		}

		$activate = activate_plugin( RMU_PLUGIN_FILE );
		// @todo: Add notice about auto-activate?
	}

	public function add_meta_boxes() {
		if ( $this->is_dismissed( 'seo_meta_box' ) ) {
			return;
		}

		if ( function_exists( 'is_gutenberg_page' ) && is_gutenberg_page() ) {
			return;
		}

		$post_types = get_post_types( array( 'public' => true ) );
		add_meta_box( 'rm-upsell-metabox', 'SEO', array( $this, 'meta_box_content' ), $post_types, 'advanced', 'high' );
	}

	public function meta_box_content() {
		$plugins      = array_keys( get_plugins() );
		$rm_installed = in_array( RMU_PLUGIN_FILE, $plugins, true );
		?>
		<div id="mts-rm-upsell-metabox">
			<?php
			if ( $rm_installed ) {
					echo strtr( $this->get_setting( 'metabox_notice_activate' ), array( '@CTA' => $this->get_activate_link() ) );
			} else {
					echo strtr( $this->get_setting( 'metabox_notice_install' ), array( '@CTA' => $this->get_install_link() ) );
			}
			?>
			<a href="#" id="mts-rm-upsell-dismiss"><span class="dashicons dashicons-no-alt"></span></a>
		</div>
		<script type="text/javascript">
			jQuery(window).load(function() {
				var $ = jQuery;
				$( '#mts-rm-upsell-dismiss' ).click(function(event) {
					event.preventDefault();
					$( '#rm-upsell-metabox' ).fadeOut( '400' );
					$.ajax({
						url: ajaxurl,
						type: 'GET',
						data: { action: 'rmu_dismiss', n: 'seo_meta_box' },
					});
				});
			});
		</script>
		<style type="text/css">
			#mts-rm-upsell-metabox{border-left:4px solid #ffba00;background:#fffbee;padding:12px 24px 12px 12px;box-shadow:0 1px 1px 0 rgba(0,0,0,.1);margin:10px 0 0;line-height:1.8;position:relative;z-index:1;}
			#mts-rm-upsell-dismiss{display:block;position:absolute;right:12px;top:24px;top:calc(50% - 12px);text-decoration:none;color:#444;}
			.mts-ctad-question{font-weight:bold;}
		</style>
		<?php
	}

	public static function init( $config = array() ) {
		if ( self::$instance === null ) {
			self::$instance = new MTS_RMU( $config );
		} else {
			self::$instance->config( $config );
		}

		return self::$instance;
	}

	public function config( $configuration, $value = null ) {
		if ( is_string( $configuration ) && $value !== null ) {
			$this->config[ $configuration ] = $value;
			return;
		}

		$this->config = array_merge( $this->config, $configuration );
	}

	public function get_setting( $setting ) {
		if ( isset( $this->config[ $setting ] ) ) {
			return $this->config[ $setting ];
		}
		return null;
	}

	public function dismiss_notice( $notice ) {
		$current            = (array) get_user_meta( get_current_user_id(), 'rmu_dismiss', true );
		$current[ $notice ] = '1';
		update_user_meta( get_current_user_id(), 'rmu_dismiss', $current );
	}

	public function is_dismissed( $notice ) {
		$current = (array) get_user_meta( get_current_user_id(), 'rmu_dismiss', true );
		return ( ! empty( $current[ $notice ] ) );
	}

	public function ajax_dismiss_notice() {
		$notice = sanitize_title( wp_unslash( $_GET['n'] ) );
		$this->dismiss_notice( $notice );
		exit;
	}

	public function inject_metabox_notice( $plugin_name, $selector, $metabox_dependency ) {
		$plugin = sanitize_title( $plugin_name );
		if ( $this->is_dismissed( $plugin ) ) {
				return;
		}

		if ( function_exists( 'is_gutenberg_page' ) && is_gutenberg_page() ) {
				return;
		}
		?>
		<div style="display: none;" id="mts-rm-upsell-notice">
			<?php echo $this->get_competitor_notice( $plugin_name ); ?>
			<a href="#" id="mts-rm-upsell-dismiss"><span class="dashicons dashicons-no-alt"></span></a>
		</div>
		<script type="text/javascript">
			jQuery(window).load(function() {
				var $ = jQuery;
				if ( $( '<?php echo $metabox_dependency; ?>' ).length ) {
					$( '#mts-rm-upsell-notice' ).<?php echo $selector; ?>.show();
					$( '#mts-rm-upsell-dismiss' ).click(function(event) {
						event.preventDefault();
						$( '#mts-rm-upsell-notice' ).fadeOut( '400' );
						$.ajax({
							url: ajaxurl,
							type: 'GET',
							data: { action: 'rmu_dismiss', n: '<?php echo $plugin; ?>' },
						});
					});
				}
			});
		</script>
		<?php
		echo $this->get_notice_css();
	}

	public function get_competitor_notice( $utm_source, $cta = true ) {
		return strtr(
			$this->get_setting( 'competitor_notice' ),
			array(
				'@CTA'    => $cta ? $this->get_install_or_activate_link() : '',
				'@SOURCE' => $utm_source,
			)
		);
	}

	public function get_notice_css() {
		return '
		<style type="text/css">
			#mts-rm-upsell-notice{border-left:4px solid #ffba00;background:#fffbee;padding:12px 24px 12px 12px;box-shadow:0 1px 1px 0 rgba(0,0,0,.1);margin:10px 0;line-height:1.8;position:relative;z-index:1;}
			#mts-rm-upsell-dismiss{display:block;position:absolute;right:4px;top:5px;text-decoration:none;color:rgba(82, 65, 0, 0.16);}
			.mts-ctad-question{font-weight:bold;}
			.nhp-opts-info-field{width:94%;}
		</style>';
	}

	public function get_install_link( $class = '', $label = '' ) {
		if ( ! $label ) {
			$label = '<strong>' . $this->get_setting( 'link_label_install' ) . '</strong>';
		}
		$action       = 'install-plugin';
		$slug         = RMU_PLUGIN_SLUG;
		$install_link = add_query_arg(
			array(
				'tab'       => 'plugin-information',
				'plugin'    => $slug,
				'TB_iframe' => 'true',
				'width'     => '600',
				'height'    => '550',
			),
			admin_url( 'plugin-install.php' )
		);

		return '<a href="' . $install_link . '" class="thickbox ' . esc_attr( $class ) . '" title="' . esc_attr__( 'Rank Math SEO', 'schema' ) . '">' . $label . '</a>';
	}

	public function enqueue_install_scripts( $hook_suffix ) {
		$load_scripts = false;

		if ( $hook_suffix == 'index.php' ) {
			if ( $this->get_setting( 'add_dashboard_widget' ) ) {
				$load_scripts = true;
			}
		} elseif ( $hook_suffix == 'post-new.php' || $hook_suffix == 'post.php' ) {
			if ( $this->get_setting( 'show_competitor_notice' ) || $this->get_setting( 'show_metabox_notice' ) ) {
				$load_scripts = true;
			}
		} elseif ( $hook_suffix == 'appearance_page_theme_options' ) {
			if ( $this->get_setting( 'add_theme_options_tab' ) ) {
				$load_scripts = true;
			}
		}

		if ( $load_scripts ) {
			add_thickbox();
			wp_enqueue_script( 'plugin-install' );
			wp_enqueue_script( 'updates' );
		}
	}

	public function get_activate_link( $class = '', $label = '' ) {
		if ( ! $label ) {
			$label = '<strong>' . $this->get_setting( 'link_label_activate' ) . '</strong>';
		}
		$activate_link = wp_nonce_url( 'plugins.php?action=activate&plugin=' . rawurlencode( RMU_PLUGIN_FILE ), 'activate-plugin_' . RMU_PLUGIN_FILE );
		return '<a href="' . $activate_link . '" class="' . esc_attr( $class ) . '">' . $label . '</a>';
	}

	public function get_install_or_activate_link( $class = '', $label_install = '', $label_activate = '' ) {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		$plugins      = array_keys( get_plugins() );
		$rm_installed = in_array( RMU_PLUGIN_FILE, $plugins, true );

		if ( ! $rm_installed ) {
			return $this->get_install_link( $class, $label_install );
		} else {
			return $this->get_activate_link( $class, $label_activate );
		}
	}

	public function inject_yoast_notice() {
		$this->inject_metabox_notice( 'Yoast+SEO', 'insertBefore("#wpseo_meta")', '#wpseo_meta' );
	}

	public function inject_aioseo_notice() {
		$this->inject_metabox_notice( 'AIO+SEO', 'insertBefore("#aiosp")', '#aiosp' );
	}

	public function add_theme_options_seo_tab( $sections ) {
		$didyouknow     = '';
		$active_plugins = get_option( 'active_plugins' );
		if ( in_array( 'wordpress-seo/wp-seo.php', $active_plugins, true ) ) {
			$didyouknow = '<div id="mts-rm-upsell-notice">' . $this->get_competitor_notice( 'Theme+Options+Panel', false ) . '</div>';
		} elseif ( in_array( 'all-in-one-seo-pack/all_in_one_seo_pack.php', $active_plugins, true ) ) {
			$didyouknow = '<div id="mts-rm-upsell-notice">' . $this->get_competitor_notice( 'Theme+Options+Panel', false ) . '</div>';
		}
		$sections[] = array(
			'icon'   => 'fa fa-line-chart',
			'title'  => __( 'SEO', 'schema' ),
			'desc'   => '<p class="description">' . __( 'Fix all your SEO issues with just a few clicks. Comply with all the latest Google\'s guidelines.', 'schema' ) . '</p>',
			'fields' => array(
				'seo_message' => array(
					'id'    => 'mts_seo_message',
					'type'  => 'info',
					'title' => '<a href="https://mythemeshop.com/plugins/wordpress-seo-plugin/?utm_source=Theme+Options+Panel&utm_medium=Link+CPC&utm_content=Rank+Math+SEO+LP&utm_campaign=UserBackend" target="_blank"><img src="data:image/svg+xml;utf8,' . esc_attr( $this->get_logo_svg() ) . '" style="margin: 10px 0 20px;" title="#1 SEO Plugin" alt="Rank Math SEO Plugin" width="431" height="112"></a>',
					'desc'  => strtr( $this->get_setting( 'theme_options_notice' ), array( '@CTA' => $this->get_install_or_activate_link() ) ) . $didyouknow . $this->get_install_or_activate_link( 'button button-primary', __( 'Install Now', 'schema' ), __( 'Activate It', 'schema' ) ) . $this->get_notice_css(),
				),
			),
		);
		return $sections;
	}

	public function get_logo_svg() {
		return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 184.23 48.28"><defs><style>.cls-1{fill:#656fb4;}.cls-1,.cls-2,.cls-3,.cls-4,.cls-5,.cls-6,.cls-7,.cls-8,.cls-9{fill-rule:evenodd;}.cls-2{fill:#071016;}.cls-3{fill:#586c7d;}.cls-4{fill:#41b5e7;}.cls-5{fill:#2c3d48;}.cls-6{fill:#0097d5;}.cls-7{fill:#006faa;}.cls-8{fill:#94a3d3;}.cls-9{fill:#7b8cc6;}</style></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M51.78,42.18V21.63h7.88a8.36,8.36,0,0,1,5.62,1.72,6.09,6.09,0,0,1,2,4.89,5.83,5.83,0,0,1-4.51,6.09l5.4,7.85H63.7l-4.9-7.24H55.48v7.24Zm3.7-10.63h4.09a5.47,5.47,0,0,0,2.94-.67,2.77,2.77,0,0,0,1.07-2.55,2.76,2.76,0,0,0-1.07-2.56,5.66,5.66,0,0,0-2.94-.66H55.48Z"/><path class="cls-1" d="M68.47,37.16a5.36,5.36,0,0,1,.09-1A4.84,4.84,0,0,1,69,35a4.24,4.24,0,0,1,1-1.3,5.82,5.82,0,0,1,2-1,10.61,10.61,0,0,1,3.06-.4,18.53,18.53,0,0,1,2.69.21c0-1.94-1.08-2.92-3.24-2.92a12.13,12.13,0,0,0-4.51.77V27.33a13.87,13.87,0,0,1,4.72-.77,7,7,0,0,1,5,1.68,6.44,6.44,0,0,1,1.74,4.85v4.16A4.92,4.92,0,0,1,80,40.9a6.79,6.79,0,0,1-4.87,1.53,7.09,7.09,0,0,1-5-1.54,5,5,0,0,1-1.6-3.73Zm3.69-.46c0,1.64,1,2.46,2.89,2.46h0c1.83,0,2.75-.77,2.75-2.33V34.89a18.33,18.33,0,0,0-2.2-.19C73.33,34.7,72.17,35.36,72.16,36.7Z"/><path class="cls-1" d="M83.63,42.18V28.25a15.51,15.51,0,0,1,7.05-1.69,7.13,7.13,0,0,1,5,1.68,6.09,6.09,0,0,1,1.81,4.7v9.24H93.83V32.63a3.86,3.86,0,0,0-.12-1,2.47,2.47,0,0,0-.45-.86,2,2,0,0,0-1.1-.68,7.23,7.23,0,0,0-2-.22,16.13,16.13,0,0,0-2.86.27V42.18Z"/><polygon class="cls-1" points="99.77 42.18 99.77 21.05 103.47 21.05 103.47 32.45 108.64 26.81 113.55 26.81 106.85 33.65 114.26 42.18 109.57 42.18 104.49 36.12 103.47 37.16 103.47 42.18 99.77 42.18"/><polygon class="cls-2" points="122.23 42.18 122.23 21.63 126.33 21.63 132.27 30.23 138.21 21.63 142.31 21.63 142.31 42.18 138.62 42.18 138.62 26.99 132.27 36.08 125.92 26.99 125.92 42.18 122.23 42.18"/><path class="cls-2" d="M144.69,37.16a4.61,4.61,0,0,1,.1-1,4.18,4.18,0,0,1,.47-1.22,4,4,0,0,1,1.05-1.3,5.67,5.67,0,0,1,2-1,10.69,10.69,0,0,1,3.07-.4,18.74,18.74,0,0,1,2.69.21c0-1.94-1.09-2.92-3.25-2.92a12.13,12.13,0,0,0-4.51.77V27.33a13.87,13.87,0,0,1,4.72-.77,6.94,6.94,0,0,1,5,1.68,6.44,6.44,0,0,1,1.74,4.85v4.16a4.89,4.89,0,0,1-1.58,3.65,6.76,6.76,0,0,1-4.86,1.53c-2.27,0-3.94-.52-5-1.54a5,5,0,0,1-1.61-3.73Zm3.7-.46c0,1.64,1,2.46,2.89,2.46h0c1.83,0,2.75-.77,2.75-2.33V34.89a18.33,18.33,0,0,0-2.2-.19C149.56,34.7,148.4,35.36,148.39,36.7Z"/><path class="cls-2" d="M158.85,30V26.81h2.58V22.28h3.7v4.53h3.64V30h-3.64v7.59a1.73,1.73,0,0,0,.41,1.34,2.37,2.37,0,0,0,1.47.34,4.42,4.42,0,0,0,1.76-.33V42a6.09,6.09,0,0,1-2.36.45h-.08a4.73,4.73,0,0,1-3.59-1.36,5.14,5.14,0,0,1-1.31-3.73V30Z"/><path class="cls-2" d="M170.61,42.18V21h3.7v6.12a8.32,8.32,0,0,1,3.08-.52,7.13,7.13,0,0,1,5,1.68,6.12,6.12,0,0,1,1.81,4.7v9.24h-3.7V32.63a5,5,0,0,0-.1-1,2.51,2.51,0,0,0-.46-.86,2.22,2.22,0,0,0-1.16-.68,7.8,7.8,0,0,0-2-.22,6.07,6.07,0,0,0-2.46.45v11.9Z"/><path class="cls-3" d="M16,30.49a1.5,1.5,0,0,0,1,.4v8.55L8.49,35.55v-.22Z"/><path class="cls-4" d="M8.49,25H17v2.84a1.53,1.53,0,0,0-1.43,1L8.49,33.45Z"/><path class="cls-5" d="M23.76,34.48a1.75,1.75,0,0,0,1.75,1.73v7.12L17,39.44V30.89a1.52,1.52,0,0,0,1-.39Z"/><path class="cls-6" d="M17,19.43h8.51V32.71a1.67,1.67,0,0,0-1.07.37l-6-4.14A1.54,1.54,0,0,0,17,27.82Z"/><path class="cls-2" d="M34,27.26v6.07l-3.76,8.23a3.61,3.61,0,0,1-4.75,1.77h0V36.21a1.75,1.75,0,0,0,1.76-1.75,1.27,1.27,0,0,0,0-.34Z"/><path class="cls-7" d="M25.51,13.77H34V25.08l-7.74,7.81a1.69,1.69,0,0,0-.78-.18Z"/><path class="cls-2" d="M14.17,6.08,3.54,29.35a5.16,5.16,0,0,0,2.54,6.8l18.8,8.59a5.14,5.14,0,0,0,6.79-2.54L42.3,18.92a5.13,5.13,0,0,0-2.53-6.79L21,3.54a5.16,5.16,0,0,0-6.8,2.54ZM.74,28.07,11.37,4.8A8.22,8.22,0,0,1,22.25.74l18.8,8.59A8.24,8.24,0,0,1,45.11,20.2L34.48,43.48A8.23,8.23,0,0,1,23.6,47.54L4.8,39A8.22,8.22,0,0,1,.74,28.07Z"/><path class="cls-3" d="M16,30.49a1.5,1.5,0,0,0,1,.4v8.55L8.49,35.55v-.22Z"/><path class="cls-8" d="M8.49,25H17v2.84a1.53,1.53,0,0,0-1.43,1L8.49,33.45Z"/><path class="cls-5" d="M23.76,34.48a1.75,1.75,0,0,0,1.75,1.73v7.12L17,39.44V30.89a1.52,1.52,0,0,0,1-.39Z"/><path class="cls-9" d="M17,19.43h8.51V32.71a1.67,1.67,0,0,0-1.07.37l-6-4.14A1.54,1.54,0,0,0,17,27.82Z"/><path class="cls-2" d="M34,27.26v6.07l-3.76,8.23a3.61,3.61,0,0,1-4.75,1.77h0V36.21a1.75,1.75,0,0,0,1.76-1.75,1.27,1.27,0,0,0,0-.34Z"/><path class="cls-1" d="M25.51,13.77H34V25.08l-7.74,7.81a1.69,1.69,0,0,0-.78-.18Z"/></g></g></svg>';
	}

	public function rm_install_redirect() {
		if ( ( isset( $_GET['tgmpa-activate'] ) && $_GET['tgmpa-activate'] == 'activate-plugin' ) || ( isset( $_POST['action'] ) && $_POST['action'] == 'tgmpa-bulk-activate' && isset( $_POST['plugin'] ) && is_array( $_POST['plugin'] ) && in_array( 'seo-by-rank-math', $_POST['plugin'] ) ) ) {
			add_action( 'admin_footer', array( $this, 'rm_redirect_late' ), 99 );
		}
	}

	public function rm_redirect_late() {
		$options           = get_option( 'mts_connect_data', false );
		$invalid           = empty( $options );
		$skip_registration = get_option( 'rank_math_registration_skip' );

		if ( true === boolval( $skip_registration ) ) {
			$invalid = false;
		}

		$url = 'wizard';
		if ( $invalid ) {
			$url = 'registration';
		} elseif ( get_option( 'rank_math_wizard_completed' ) ) {
			$url = '';
		}

		if ( $url ) {
			$url = add_query_arg( array( 'page' => 'rank-math-' . $url ), admin_url( 'admin.php' ) );
			echo '<div class="rm-redirect-wrap" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: rgba(255, 255, 255, 0.4); z-index: 111112;"><div class="rm-redirect-notice" style="position: absolute; top: 20%; left: 50%; width: 400px; margin-left: -200px; text-align: center; background: #fff; box-shadow: 0 10px 100px rgba(0, 0, 0, 0.3); padding-top: 35px; padding-bottom: 40px;">' . __( 'Redirecting to Rank Math Setup Wizard...', 'schema' ) . '</div></div>';
			echo '<meta http-equiv="refresh" content="0;URL=\'' . $url . '\'" />';
		}
	}
}

define( 'RMU_ACTIVE', true );
