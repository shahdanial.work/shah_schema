<?php
/**
 * The template for displaying search results pages.
 *
 * @package Schema
 */

$mts_options = get_option(MTS_THEME_NAME);

get_header();
?>

<section id="page" class="style_width style_content_sidebar">
		<?php
        // Elementor `archive` location.
        if (! function_exists('elementor_theme_do_location') || ! elementor_theme_do_location('archive')) {
            ?>
			<div id="content_box" class="content">
				<header class="page-header style_archieve_heading">
					<i class="fa fa-search" aria-hidden="true"></i><h1 class="page-title"><?php esc_html_e('Carian', 'schema'); ?>  <?php the_search_query(); ?></h1>
				</header>
				<?php
                $j = 0;
            if (have_posts()) :
                    while (have_posts()) :
                        the_post(); ?>
						<article class="latestPost excerpt">
							<?php mts_archive_post(); ?>
						</article><!--.post excerpt-->
					<?php
                endwhile; else :
                ?>
				<div class="no-results">
					<h2><?php esc_html_e('We apologize for any inconvenience, please hit back on your browser or use the search form below.', 'schema'); ?></h2>
					<?php get_search_form(); ?>
				</div><!--noResults-->
				<?php
            endif;

            ++$j;
            if (0 !== $j) { // No pagination if there is no posts.
                mts_pagination();
            } ?>
			</div>
			<?php
        }
        ?>
	<?php
        get_sidebar();
    echo '</section>';
    get_footer();
