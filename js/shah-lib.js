
  //////////////////////////////////// MAIN FUNCTION ///////////////////////////////////

  function dearray_cookies(arr) {
    var arr = cleanArray(arr);
    var str = "";
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].length > 0) {
        if (arr[i].trim().length > 0) str += arr[i];
        if (i != last_array(arr)) str += "_,_";
      }
    }
    return str;
  }

  function rearray_cookies(wtf) {
    if (wtf) {
      if (wtf.length > 0) return wtf.split("_,_").reverse();
    } else {
      return [];
    }
  }

  function set_cookie(nm, val, brphr) {
    var d = new Date();
    d.setTime(d.getTime() + brphr * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toGMTString();
    document.cookie =
      nm + "=" + val + ";" + expires + ";path=/;domain=malayatimes.com;";
  }

  function get_cookie(kkk) {
    var myt_cookies = {};
    var all_cock = document.cookie.split(";");

    if (all_cock) {
      for (var i = 0; i < all_cock.length; i++) {
        var cocki = all_cock[i].split("=");
        if (cocki[0] && cocki[1]) {
          if (cocki[0].length > 0 && cocki[1].length > 0) {
            myt_cookies[cocki[0].trim()] = cocki[1].trim();
          }
        }
      }
    }
    if (kkk && myt_cookies[kkk]) {
      return myt_cookies[kkk];
    } else {
      return false;
    }
  }

  function check_attribute(a, attr, valu) {
    if (attr == "tagName") {
      if (a.tagName.toLowerCase() == valu.toLowerCase()) {
        return a.tagName;
      } else {
        return false;
      }
    } else {
      var vall = " " + valu + " ";
      if (a.nodeType === 1 && a.getAttribute(attr)) {
        var get_attr = " " + a.getAttribute(attr) + " ";
        if (get_attr.indexOf(vall) > -1) {
          return a.getAttribute(attr);
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  function add_class(elem, new_class) {
    if (!elem) return false;
    if (elem.className.length > 0) {
      var klas = " " + elem.className + " ";
      if (!klas.indexOf(" " + new_class + " ").length > -1) {
        elem.className = elem.className.trim() + " " + new_class;
      }
    } else {
      elem.className = new_class;
    }
  }

  function remove_class(elem, target_class) {
    /*target_class = className yg nak remove dari list of class */
    if (elem) {
      if (elem.className.length > 0) {
        var classes = elem.className.trim().split(" ");
        elem.removeAttribute("class");
        new_class = "";
        for (var i = 0; i < classes.length; i++) {
          if (classes[i] != target_class.trim()) {
            new_class = new_class + classes[i] + " ";
          }
        }
        if (new_class.trim().length > 0) {
          elem.className = new_class.trim();
        }
      } else {
        elem.removeAttribute("class");
      }
    }
  }

  function last_array(arr) {
    if (arr) {
      return parseFloat(arr.length) - parseFloat(1);
    } else {
      return false;
    }
  }

  function get_domain(str) {
    var str = str.replace(/http:|https:/g, "").replace("//", "");
    if (str.indexOf("/") > -1) {
      var str = str.split("/");
      var str = str[0];
    }
    if (str.indexOf("?") > -1) {
      var str = str.split("?");
      var str = str[0];
    }
    if (str.indexOf("#") > -1) {
      var str = str.split("#");
      var str = str[0];
    }
    return str.toLowerCase();
  }

  function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
      if (newArray.indexOf(actual[i]) < 0) newArray.push(actual[i]);
    }
    return newArray;
  }

  function is_visible(el) {
    if (!el) return false;

    var supportPageOffset = window.pageXOffset !== undefined;
    var isCSS1Compat = (document.compatMode || "") === "CSS1Compat";
    var xxx =
      typeof window.scrollX !== undefined
        ? window.scrollX
        : document.body.scrollLeft;
    var yyy =
      typeof window.scrollX !== undefined
        ? window.scrollX
        : document.body.scrollTop;

    var window_scroll_x = supportPageOffset
      ? window.pageXOffset
      : isCSS1Compat
      ? document.documentElement.scrollLeft
      : xxx;

    var window_scroll_y = supportPageOffset
      ? window.pageYOffset
      : isCSS1Compat
      ? document.documentElement.scrollTop
      : yyy;

    var screen_height =
      typeof document.documentElement.clientHeight !== undefined
        ? document.documentElement.clientHeight
        : window.innerHeight;

    var screen_width =
      typeof document.documentElement.clientWidth !== undefined
        ? document.documentElement.clientWidth
        : window.innerWidth;

    return (
      el.offsetTop <= screen_height + window_scroll_y && //el.dibawah
      window_scroll_y <= el.clientHeight + el.offsetTop && //window_scroll_y <= el.clientHeight + el.offsetTop + screen_height //el.diatas
      el.offsetLeft <= screen_width && //el.kanan
      el.offsetLeft + el.clientWidth >= 0
    ); //e.dikiri
  }

  function strHtmlToDom(MYSTR) {
    /* EX: strHtmlToDom('<div class="wtf">HAHAHAHA</div>') */
    var div = MYSTR,
      parser = new DOMParser(),
      doc = parser.parseFromString(MYSTR, "text/xml");
    return doc.firstChild;
  }

  function placeCaretAtEnd(el, dimana) {
    // dimana: true = atStart, false = atEnd;
    if (
      typeof window.getSelection != "undefined" &&
      typeof document.createRange != "undefined"
    ) {
      var range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(dimana);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
      var textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(dimana);
      textRange.select();
    }
  }

  function url_is_image(str) {
    if (str.indexOf(".") < 0) {
      return false;
    } else {
      str = str.split(".").reverse();
      str = str[0].toLowerCase();
      return str == "jpg" || str == "png" || str == "jpeg" || "gif";
    }
  }


  function get_query_url(key) {
    return decodeURIComponent(
      window.location.search.replace(
        new RegExp(
          "^(?:.*[&\\?]" +
            encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") +
            "(?:\\=([^&]*))?)?.*$",
          "i"
        ),
        "$1"
      )
    );
  }

  function get_wrapper(a, attr, valu) {
    var vall = " " + valu + " ";
    var c = false;
    var d = false;
    var w = a;

    while ((a && !c) || !d) {
      if (a != w) {
        if (
          a.nodeType === 1 &&
          a.tagName &&
          a.tagName != null &&
          typeof a.tagName !== "undefined"
        ) {
          if (a.tagName.toLowerCase() == "body") {
            // stop while when reach body...
            d = true;
          }
        }
        if (a.nodeType === 1 && a.getAttribute(attr)) {
          var get_attr = " " + a.getAttribute(attr) + " ";
          if (get_attr.indexOf(vall) > -1) {
            c = true; // stop while.
            return a;
          }
        }
      }
      a = a.parentNode;
    }
    if (!c) {
      // jika element takde.
      return false;
    }
  }

  function autocomplete(keyword, arr) {
    var res = [];

    for (i = 0; i < arr.length; i++) {
      if (
        keyword.toLowerCase() === arr[i].toLowerCase().slice(0, keyword.length)
      ) {
        res.push(arr[i]);
      }
    }

    return res;
  }