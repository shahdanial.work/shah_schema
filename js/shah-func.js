var bodyElem = document.getElementById('blog') || document.body;

/* close telegram bar */
var telegramBar = document.getElementById('top_ad_nav');
if(telegramBar && bodyElem){
  telegramBar.addEventListener('click', function(){
    add_class(bodyElem, 'no_telegram_bar');
  });
}

/* toggle nav (toggle class) */
var menuButton =  document.getElementById('pull');
if(menuButton && menuButton){
  menuButton.addEventListener('click', function(){
    var toggleCLassName = 'mobile-menu-active';
    if(check_attribute(bodyElem, 'class', toggleCLassName)){
      // remove class
      remove_class(bodyElem, toggleCLassName);
    } else {
      // add class
      add_class(bodyElem, toggleCLassName);
    }
  });
}