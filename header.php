<?php
/**
 * The template for displaying the header.
 *
 * Displays everything from the doctype declaration down to the navigation.
 *
 * @package Schema
 */
$mts_options = get_option( MTS_THEME_NAME );
$header_class = $mts_options['mts_header_style'];
$show_header  = '';
// $featuredImage = wp_get_attachment_image_src( get_the_ID(), 'medium' );
if ( is_singular() ) {
	$show_header = get_post_meta( get_the_ID(), '_disable_header', true );
}
#----------- SETTINGS DYNAMIC ----------
								$aStyle = '';
								$shah_logo = wp_get_attachment_image_src( $mts_options['mts_logo'], 'full' );
								$headerStyle = '';
								$sectionStyle = '';
								if($shah_logo){
									if($shah_logo[0]){
										$aStyle = ' style="background:url(' . esc_url( $shah_logo[0] ) . ') no-repeat;text-indent:-9999px;width:100%;height:100%;color:rgba(0,0,0,0);display:block;margin:auto;background-position: center center;';
										if($shah_logo[1]){
												$headerStyle .= 'style="width:'.esc_attr($shah_logo[1]).'px;';
										}
										if($shah_logo[2]){
											  $theHeight = $shah_logo[2] + 10;
												$headerStyle .= 'height:'.esc_attr($theHeight).'px;';
												$sectionStyle .= ' style="height:'.esc_attr($theHeight).'px;';
										}
										$sectionStyle .= '"';
										$headerStyle .= '"';
										$aStyle .= '"';
									}
								}
								// print_r('tryy');
								// print_r($sectionStyle);
								// print_r($headerStyle);
								// print_r($aStyle);
								// print_r($mts_logo[0]);
								// print_r('<br>');
								// print_r($headerStyle);
?>
<!DOCTYPE html>
<!-- optimized by Shah Danial -->
<html class="no-js" <?php language_attributes(); ?>>

<head itemscope itemtype="http://schema.org/WebSite">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php mts_meta(); ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!-- start item built by wp_head() -->
	<?php wp_head(); ?>
	<!-- end item built by wp_head() -->
	<?php
		// if($featuredImage) echo '<meta property="og:image" content="'. $featuredImage .'">';
		if(postFeturedUrl(get_the_ID())){
			echo '<meta property="og:image" content="'. postFeturedUrl(get_the_ID()) .'">';
		}
	?>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="#4267b2"/>
</head>

<body id="blog" <?php body_class( 'main' ); ?> itemscope itemtype="http://schema.org/WebPage">

	<?php
		#----------------------- TOP AD NAV ------------------------
	?>
	<aside class="not-important" id="top_ad_nav">
			<div class="style_width inner_wrapper top_ad_nav">
    		<div class="item">Dapatkan update jawatan menarik <a rel="nofollow" href="https://t.me/jawatan" target="_blank">JOIN TELEGRAM</a></div>
			</div>
		</aside>
<?php
		if ( empty( $show_header ) ) {
			?>
			<section class="style_header_wrapper style_nav_wrapper"<?php echo $sectionStyle ?>><div class="style_width inner_wrapper">
				<?php
				// Elementor `header` location.
				if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'header' ) ) {

					#-------------------------------------------

					if ( '1' === $mts_options['mts_show_primary_nav'] ) {
						?>
						<nav id="site-navigation">
							<div class="container">
								<div id="primary-navigation" class="primary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
									<div class="navigation clearfix">
										<?php
										if ( has_nav_menu( 'primary-menu' ) ) {
											wp_nav_menu( array(
												'theme_location' => 'primary-menu',
												'menu_class' => 'menu clearfix',
												'container' => '',
												'walker' => new mts_menu_walker(),
											) );
										} else {
											?>
											<ul class="menu clearfix">
												<?php wp_list_pages( 'title_li=' ); ?>
											</ul>
										<?php } ?>
										<?php if ( '1' === $mts_options['mts_header_social_icons'] && ! empty( $mts_options['mts_header_social'] ) && is_array( $mts_options['mts_header_social'] ) ) { ?>
											<div class="header-social-icons">
											<?php foreach ( $mts_options['mts_header_social'] as $header_icons ) : ?>
												<?php if ( ! empty( $header_icons['mts_header_icon'] ) && isset( $header_icons['mts_header_icon'] ) ) : ?>
													<a href="<?php echo esc_url( $header_icons['mts_header_icon_link'] ); ?>" class="header-<?php echo esc_attr( $header_icons['mts_header_icon'] ); ?>" target="_blank">
														<span class="fa fa-<?php echo esc_attr( $header_icons['mts_header_icon'] ); ?>"></span>
													</a>
												<?php endif; ?>
											<?php endforeach; ?>
											</div>
										<?php } ?>
										<?php mts_cart(); ?>
									</div>
								</div>
							</div>
						</nav>
						<?php
					}

					#-------------------------------------------

					if ( 'regular_header' === $mts_options['mts_header_style'] ) {
						?>
						<div id="regular-header">
							<div class="container">
								<div class="logo-wrap <?php echo esc_attr( $header_class ); ?>" role="banner" itemscope itemtype="http://schema.org/WPHeader">
									<?php
									$mts_logo = wp_get_attachment_image_src( $mts_options['mts_logo'], 'full' );
									if ( '' !== $mts_options['mts_logo'] && $mts_logo ) {
										if ( is_home() || is_404() ) {
											?>
											<h1 id="logo" class="site-title" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( $mts_logo[0] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="<?php echo esc_attr( $mts_logo[1] ); ?>" height="<?php echo esc_attr( $mts_logo[2] ); ?>"></a>
											</h1><!-- END #logo -->
											<?php
										} else {
											?>
											<h2 id="logo" class="site-title" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"><?php echo esc_attr( get_bloginfo( 'name' ) ); ?></a>
											</h2><!-- END #logo -->
											<?php
										}
									} else {
										if ( is_home() || is_404() ) {
											?>
											<h1 id="logo" class="text-logo" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
											</h1><!-- END #logo -->
											<?php
										} else {
											?>
											<h2 id="logo" class="text-logo" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
											</h2><!-- END #logo -->
											<?php
										}
									}
									?>
								</div>
								<?php if ( '' !== $mts_options['mts_header_adcode'] ) { ?>
									<div class="widget-header"><?php echo $mts_options['mts_header_adcode']; // PHPCS:ignore ?></div>
								<?php } ?>
							</div>
						</div>
						<?php
					}

					#-------------------------------------------

					if ( '1' === $mts_options['mts_sticky_nav'] ) {
						?>
						<div class="clear" id="catcher"></div>
						<div id="header" class="sticky-navigation">
						<?php
					} else {
						?>
						<?php
					}
					?>
							<?php if ( 'logo_in_nav_header' === $mts_options['mts_header_style'] ) { ?>
							<?php
								// #----------- SETTINGS DYNAMIC ----------
								// $aStyle = '';
								// $shah_logo = wp_get_attachment_image_src( $mts_options['mts_logo'], 'full' );
								// $headerStyle = '';
								// if($shah_logo){
								// 	if($shah_logo[0]){
								// 		$aStyle = ' style="background:url(' . esc_url( $shah_logo[0] ) . ') no-repeat;text-indent:-9999px;width:100%;height:100%;color:rgba(0,0,0,0);display:block;margin:auto;background-position: center center;';
								// 		if($shah_logo[1]){
								// 				$headerStyle .= 'style="width:'.esc_attr($shah_logo[1]).'px;';
								// 		}
								// 		if($shah_logo[2]){
								// 				$headerStyle .= 'height:'.esc_attr($shah_logo[2]).'px;';
								// 		}
								// 		$headerStyle .= '"';
								// 		$aStyle .= '"';
								// 	}
								// }
								// // print_r($mts_logo[0]);
								// // print_r('<br>');
								// // print_r($headerStyle);
							?>
								<header class="site-header <?php echo esc_attr( $header_class ); ?>" role="banner" itemscope itemtype="http://schema.org/WPHeader"<?php echo $headerStyle?>>
									<?php
									$mts_logo = $shah_logo;
									if ( '' !== $mts_options['mts_logo'] && $mts_logo ) {
										if ( is_front_page() || is_home() || is_404() ) {
											?>
											<h1 id="site-title" class="site-title" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"<?php echo $aStyle?>><?php echo bloginfo( 'name' ) ?></a>
											</h1><!-- END #logo -->
											<?php
										} else {
											?>
											<h2 id="site-title" class="site-title" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"<?php echo $aStyle?>><?php echo bloginfo( 'name' ) ?></a>
											</h2><!-- END #logo -->
											<?php
										}
									} else {
										if ( is_front_page() || is_home() || is_404() ) {
											?>
											<h1 id="site-title" class="site-title" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"<?php echo $aStyle?>><?php bloginfo( 'name' ); ?></a>
											</h1><!-- END #logo -->
											<?php
										} else {
											?>
											<h2 id="logo" class="text-logo" itemprop="headline">
												<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
											</h2><!-- END #logo -->
											<?php
										}
									}
									?>
									<p class="site-description"><?php echo esc_attr( bloginfo( 'description' ) ) ?></p>
								</header>
							<?php } ?>

							<nav id="site-navigation" class="site-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
								<?php if ( has_nav_menu( 'mobile' ) ) { ?>
										<?php
										if ( has_nav_menu( 'secondary-menu' ) ) {
											wp_nav_menu( array(
												'theme_location' => 'secondary-menu',
												'menu_class' => 'menu clearfix',
												'container' => '',
												'walker' => new mts_menu_walker(),
											) );
										} else {
											?>
											<ul class="menu clearfix">
												<?php wp_list_categories( 'title_li=' ); ?>
											</ul>
											<?php
										}
										?>
										<?php
										wp_nav_menu( array(
											'theme_location' => 'mobile',
											'menu_class' => 'menu clearfix',
											'container'  => '',
											'walker'     => new mts_menu_walker(),
										) );
										?>
								<?php } else { ?>
										<?php
										if ( has_nav_menu( 'secondary-menu' ) ) {
											wp_nav_menu( array(
												'theme_location' => 'secondary-menu',
												'menu_class' => 'menu clearfix',
												'container' => '',
												'walker' => new mts_menu_walker(),
											) );
										} else {
											?>
											<ul class="menu clearfix">
												<?php wp_list_categories( 'title_li=' ); ?>
											</ul>
											<?php
										}
										?>
								<?php } ?>
								<span href="#" id="pull" class="toggle-mobile-menu"></span>
									</nav>
					<?php
				}
				?>
			</div></section>

			<?php
			if ( 'logo_in_nav_header' === $mts_options['mts_header_style'] && '' !== $mts_options['mts_header_adcode'] ) {
				?>
				<aside class="aside not-important after_header_aside">
					<div class="widget-header style_width">
					<?php echo $mts_options['mts_header_adcode']; // PHPCS:ignore ?></div>
				</aside>
				<?php
			}
		}

		/* IF YOU WISH TO ADD SEARCH BOX AT TOP (AFTER HEADER), here is the HTML, just plug and play this code..
		//--------------------------
		<div id="search-top" class="widget widget_search"><form method="get" id="searchformtop" class="search-form" action="https://www.kerjakosong.co" _lpchecked="1"><fieldset>
<input type="text" name="s" id="s" value="" placeholder="Carian jawatan..">
<button id="search_image_button" class="sbutton" type="submit" value="Search"></button></fieldset></form></div>
	  */
